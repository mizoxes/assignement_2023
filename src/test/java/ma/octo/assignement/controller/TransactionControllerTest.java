package ma.octo.assignement.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exception.CompteNonExistantException;
import ma.octo.assignement.exception.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exception.TransactionException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class TransactionControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private TransferDto transfer;
    private DepositDto deposit;

    @BeforeEach
    public void beforeEachTest() {
        transfer = new TransferDto("010000B025001000", "010000A000001000", "motif", new BigDecimal(100));
        deposit = new DepositDto("hamza", "RIB1", "some motif", new BigDecimal(10000));
    }

    @AfterEach
    public void afterEachTest() {
        transfer = null;
    }

    @Test
    public void shouldGetAllTransfers() throws Exception {
        mockMvc.perform(get("/transaction/listerTransferts").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void shouldGetAllDeposits() throws Exception {
        mockMvc.perform(get("/transaction/listerDeposits").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void shouldSucceedDeposit() throws Exception {
        mockMvc.perform(
                        post("/transaction/executerDeposit")
                                .content(asJsonString(deposit))
                                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    public void shouldSucceedTransfer() throws Exception {
        mockMvc.perform(
                post("/transaction/executerTransfert")
                    .content(asJsonString(transfer))
                    .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    public void shouldFailTransfer_SoldeInsuffisant() throws Exception {
        transfer.setMontant(new BigDecimal(10000));

        mockMvc.perform(
                        post("/transaction/executerTransfert")
                                .content(asJsonString(transfer))
                                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof SoldeDisponibleInsuffisantException))
                .andExpect(result -> assertEquals("Solde insuffisant", result.getResponse().getContentAsString()));
    }

    @Test
    public void shouldFailTransfer_MinMontant() throws Exception {
        transfer.setMontant(new BigDecimal(5));

        mockMvc.perform(
                post("/transaction/executerTransfert")
                    .content(asJsonString(transfer))
                    .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof TransactionException))
                .andExpect(result -> assertEquals("Montant minimal de transfer non atteint", result.getResolvedException().getMessage()));
    }

    @Test
    public void shouldFailTransfer_MaxMontant() throws Exception {
        transfer.setMontant(new BigDecimal(10001));

        mockMvc.perform(
                        post("/transaction/executerTransfert")
                                .content(asJsonString(transfer))
                                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof TransactionException))
                .andExpect(result -> assertEquals("Montant maximal de transfer dépassé", result.getResolvedException().getMessage()));
    }

    @Test
    public void shouldFailTransfer_MontantVide() throws Exception {
        transfer.setMontant(null);

        mockMvc.perform(
                        post("/transaction/executerTransfert")
                                .content(asJsonString(transfer))
                                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof TransactionException))
                .andExpect(result -> assertEquals("Montant vide", result.getResolvedException().getMessage()));
    }

    @Test
    public void shouldFailTransfer_MotifVide() throws Exception {
        transfer.setMotif(null);

        mockMvc.perform(
                        post("/transaction/executerTransfert")
                                .content(asJsonString(transfer))
                                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof TransactionException))
                .andExpect(result -> assertEquals("Motif vide", result.getResolvedException().getMessage()));
    }

    @Test
    public void shouldFailTransfer_BadAccount() throws Exception {
        transfer.setNrCompteEmetteur("some bad account");

        mockMvc.perform(
                        post("/transaction/executerTransfert")
                                .content(asJsonString(transfer))
                                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof CompteNonExistantException))
                .andExpect(result -> assertEquals("Compte introuvable", result.getResponse().getContentAsString()));
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
