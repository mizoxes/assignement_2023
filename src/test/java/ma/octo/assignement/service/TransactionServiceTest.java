package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.TransactionDeposit;
import ma.octo.assignement.domain.TransactionTransfer;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exception.CompteNonExistantException;
import ma.octo.assignement.exception.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exception.TransactionException;
import ma.octo.assignement.repository.*;
import ma.octo.assignement.service.impl.TransactionServiceImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class TransactionServiceTest {

    @InjectMocks
    private TransactionServiceImpl transactionService;
    @Mock
    private TransactionDepositRepository depositRepository;
    @Mock
    private TransactionTransferRepository transferRepository;
    @Mock
    private CompteRepository compteRepository;
    @Mock
    private AuditRepository auditRepository;

    private Compte compteEmetteur;
    private Compte compteBeneficiaire;
    private TransferDto transferDto;

    @BeforeEach
    public void beforeEachTest() {
        compteEmetteur = new Compte();
        compteEmetteur.setNrCompte("emetteur");
        compteEmetteur.setSolde(new BigDecimal(1000));

        compteBeneficiaire = new Compte();
        compteBeneficiaire.setNrCompte("beneficiaire");
        compteBeneficiaire.setSolde(new BigDecimal(1000));

        transferDto = new TransferDto();
        transferDto.setNrCompteEmetteur(compteEmetteur.getNrCompte());
        transferDto.setNrCompteBeneficiaire(compteBeneficiaire.getNrCompte());
        transferDto.setMontant(new BigDecimal(100));
        transferDto.setMotif("motif");
    }

    @AfterEach
    public void afterEachTest() {
        compteEmetteur = null;
        compteBeneficiaire = null;
        transferDto = null;
    }

    @Test
    void shouldGetAllDeposits() {
        List<TransactionDeposit> deposits = new ArrayList<>();
        deposits.add(new TransactionDeposit());
        deposits.add(new TransactionDeposit());

        given(depositRepository.findAll()).willReturn(deposits);

        assertEquals(deposits, transactionService.getAllDeposits());

        verify(depositRepository).findAll();
    }

    @Test
    void shouldGetAllTransfers() {
        List<TransactionTransfer> transfers = new ArrayList<>();
        transfers.add(new TransactionTransfer());
        transfers.add(new TransactionTransfer());

        given(transferRepository.findAll()).willReturn(transfers);

        assertEquals(transfers, transactionService.getAllTransfers());

        verify(transferRepository).findAll();
    }

    @Test
    void shouldSucceedTransfer() {
        given(compteRepository.findByNrCompte(compteEmetteur.getNrCompte())).willReturn(compteEmetteur);
        given(compteRepository.findByNrCompte(compteBeneficiaire.getNrCompte())).willReturn(compteBeneficiaire);

        assertDoesNotThrow(() -> transactionService.doTransfer(transferDto));

        assertEquals(compteEmetteur.getSolde(), new BigDecimal(900));
        assertEquals(compteBeneficiaire.getSolde(), new BigDecimal(1100));
        verify(compteRepository).save(compteEmetteur);
        verify(compteRepository).save(compteBeneficiaire);
        verify(transferRepository).save(any());
        verify(auditRepository).save(any());
    }

    @Test
    void shouldFailTransfer_MotifVide() {
        transferDto.setMotif("");

        given(compteRepository.findByNrCompte(compteEmetteur.getNrCompte())).willReturn(compteEmetteur);
        given(compteRepository.findByNrCompte(compteBeneficiaire.getNrCompte())).willReturn(compteBeneficiaire);

        TransactionException ex = assertThrows(TransactionException.class, () -> transactionService.doTransfer(transferDto));
        assertTrue(ex.getMessage().contains("Motif vide"));
    }

    @Test
    void shouldFailTransfer_MontantVide() {
        transferDto.setMontant(null);

        given(compteRepository.findByNrCompte(compteEmetteur.getNrCompte())).willReturn(compteEmetteur);
        given(compteRepository.findByNrCompte(compteBeneficiaire.getNrCompte())).willReturn(compteBeneficiaire);

        TransactionException ex = assertThrows(TransactionException.class, () -> transactionService.doTransfer(transferDto));
        assertTrue(ex.getMessage().contains("Montant vide"));
    }

    @Test
    void shouldFailTransfer_MaxMontant() {
        compteEmetteur.setSolde(new BigDecimal(100000));
        transferDto.setMontant(new BigDecimal(999999));

        given(compteRepository.findByNrCompte(compteEmetteur.getNrCompte())).willReturn(compteEmetteur);
        given(compteRepository.findByNrCompte(compteBeneficiaire.getNrCompte())).willReturn(compteBeneficiaire);

        TransactionException ex = assertThrows(TransactionException.class, () -> transactionService.doTransfer(transferDto));
        assertTrue(ex.getMessage().contains("Montant maximal de transfer dépassé"));
    }

    @Test
    void shouldFailTransfer_MinMontant() {
        transferDto.setMontant(new BigDecimal(5));

        given(compteRepository.findByNrCompte(compteEmetteur.getNrCompte())).willReturn(compteEmetteur);
        given(compteRepository.findByNrCompte(compteBeneficiaire.getNrCompte())).willReturn(compteBeneficiaire);

        TransactionException ex = assertThrows(TransactionException.class, () -> transactionService.doTransfer(transferDto));
        assertTrue(ex.getMessage().contains("Montant minimal de transfer non atteint"));
    }

    @Test
    void shouldFailTransfer_SoldeInsuffisant() {
        compteEmetteur.setSolde(new BigDecimal(100));
        transferDto.setMontant(new BigDecimal(5000));

        given(compteRepository.findByNrCompte(compteEmetteur.getNrCompte())).willReturn(compteEmetteur);
        given(compteRepository.findByNrCompte(compteBeneficiaire.getNrCompte())).willReturn(compteBeneficiaire);

        assertThrows(SoldeDisponibleInsuffisantException.class, () -> transactionService.doTransfer(transferDto));
    }

    @Test
    void shouldFailTransfer_BadAccountEmetteur() {
        given(compteRepository.findByNrCompte(compteEmetteur.getNrCompte())).willReturn(null);
        given(compteRepository.findByNrCompte(compteBeneficiaire.getNrCompte())).willReturn(compteBeneficiaire);

        assertThrows(CompteNonExistantException.class, () -> transactionService.doTransfer(transferDto));
    }

    @Test
    void shouldFailTransfer_BadAccountBeneficiare() {
        given(compteRepository.findByNrCompte(compteEmetteur.getNrCompte())).willReturn(compteEmetteur);
        given(compteRepository.findByNrCompte(compteBeneficiaire.getNrCompte())).willReturn(null);

        assertThrows(CompteNonExistantException.class, () -> transactionService.doTransfer(transferDto));
    }

    @Test
    void shouldSucceedDeposit() {
        String emetteur = "emetteur";
        Compte compteBeneficiaire = new Compte();
        compteBeneficiaire.setRib("beneficiaire");
        compteBeneficiaire.setSolde(new BigDecimal(1000));

        given(compteRepository.findByRib(compteBeneficiaire.getRib())).willReturn(compteBeneficiaire);

        DepositDto depositDto = new DepositDto();
        depositDto.setRib(compteBeneficiaire.getRib());
        depositDto.setMontant(new BigDecimal(100));
        depositDto.setMotif("motif");

        assertDoesNotThrow(() -> transactionService.doDeposit(depositDto));

        assertEquals(compteBeneficiaire.getSolde(), new BigDecimal(1100));
        verify(compteRepository).save(compteBeneficiaire);
        verify(depositRepository).save(any());
        verify(auditRepository).save(any());
    }

}
