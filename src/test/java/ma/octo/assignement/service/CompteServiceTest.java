package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.service.impl.CompteServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class CompteServiceTest {

    @InjectMocks
    private CompteServiceImpl compteService;

    @Mock
    private CompteRepository compteRepository;

    @Test
    void shouldGetCompteByNrCompte() {
        Compte compte = new Compte();
        compte.setNrCompte("010000A000001000");

        given(compteRepository.findByNrCompte(compte.getNrCompte())).willReturn(compte);

        assertEquals(compte, compteService.getByNrCompte(compte.getNrCompte()));

        verify(compteRepository).findByNrCompte(compte.getNrCompte());
    }

    @Test
    void shouldGetAllComptes() {
        List<Compte> comptes = new ArrayList<>();
        comptes.add(new Compte());
        comptes.add(new Compte());

        given(compteRepository.findAll()).willReturn(comptes);

        assertEquals(comptes, compteService.getAll());

        verify(compteRepository).findAll();
    }

}
