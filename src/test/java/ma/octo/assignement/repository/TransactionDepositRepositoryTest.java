package ma.octo.assignement.repository;

import ma.octo.assignement.domain.TransactionDeposit;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class TransactionDepositRepositoryTest {

    @Autowired
    private TransactionDepositRepository depositRepository;

    private TransactionDeposit deposit1;

    @BeforeEach
    public void beforeEachTest() {
        deposit1 = new TransactionDeposit();
        deposit1.setMontant(BigDecimal.valueOf(1000));
        depositRepository.deleteAll();
        depositRepository.save(deposit1);
        System.out.println(deposit1.getId());
    }

    @AfterEach
    public void afterEachTest() {
        deposit1 = null;
    }

    @Test
    public void findOne() {
        Optional<TransactionDeposit> result = depositRepository.findById(deposit1.getId());

        assertTrue(result.isPresent());
        assertEquals(deposit1.getId(), result.get().getId());
    }

    @Test
    public void findAll() {
        TransactionDeposit transfer2 = new TransactionDeposit();
        transfer2.setMontant(BigDecimal.valueOf(2000));
        depositRepository.save(transfer2);

        List<TransactionDeposit> result = depositRepository.findAll();

        assertNotNull(result);
        assertEquals(2, result.size());
    }

    @Test
    public void save() {
        TransactionDeposit transfer = new TransactionDeposit();
        transfer.setMontant(BigDecimal.valueOf(1234));

        TransactionDeposit saved = depositRepository.save(transfer);

        assertEquals(saved.getMontant().intValue(), 1234);
    }

    @Test
    public void deleteById() {
        depositRepository.deleteById(deposit1.getId());

        Optional<TransactionDeposit> result = depositRepository.findById(deposit1.getId());

        assertFalse(result.isPresent());
    }

}