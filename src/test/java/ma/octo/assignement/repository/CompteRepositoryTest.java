package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class CompteRepositoryTest {

    @Autowired
    private CompteRepository compteRepository;

    private Compte compte1;

    @BeforeEach
    public void beforeEachTest() {
        compteRepository.deleteAll();
        compte1 = new Compte();
        compte1.setNrCompte("compte1");
        compteRepository.save(compte1);
        System.out.println(compte1.getId());
    }

    @AfterEach
    public void afterEachTest() {
        compte1 = null;
    }

    @Test
    public void findOne() {
        Optional<Compte> result = compteRepository.findById(compte1.getId());

        assertTrue(result.isPresent());
        assertEquals(compte1.getId(), result.get().getId());
    }

    @Test
    public void findAll() {
        Compte compte2 = new Compte();
        compte2.setNrCompte("compte2");
        compteRepository.save(compte2);

        List<Compte> result = compteRepository.findAll();

        assertNotNull(result);
        assertEquals(2, result.size());
    }

    @Test
    public void save() {
        Compte compte = new Compte();
        compte.setNrCompte("compte2");

        Compte saved = compteRepository.save(compte);

        assertEquals(saved.getNrCompte(), "compte2");
    }

    @Test
    public void deleteById() {
        compteRepository.deleteById(compte1.getId());

        assertThat(compteRepository.count()).isZero();
    }

}