package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class UtilisateurRepositoryTest {

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    private Utilisateur utilisateur1;

    @BeforeEach
    public void beforeEachTest() {
        utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("username1");
        utilisateur1.setFirstname("firstname1");
        utilisateur1.setLastname("lastname1");
        utilisateur1.setGender("Male");
        utilisateurRepository.deleteAll();
        utilisateurRepository.save(utilisateur1);
    }

    @AfterEach
    public void afterEachTest() {
        utilisateur1 = null;
    }

    @Test
    public void findOne() {
        Optional<Utilisateur> result = utilisateurRepository.findById(utilisateur1.getId());

        assertTrue(result.isPresent());
        assertEquals(utilisateur1.getId(), result.get().getId());
    }

    @Test
    public void findAll() {
        Utilisateur utilisateur2 = new Utilisateur();
        utilisateur2.setUsername("username2");
        utilisateur2.setFirstname("firstname2");
        utilisateur2.setLastname("lastname2");
        utilisateur2.setGender("Female");
        utilisateurRepository.save(utilisateur2);

        List<Utilisateur> result = utilisateurRepository.findAll();

        assertNotNull(result);
        assertEquals(2, result.size());
    }

    @Test
    public void save() {
        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setUsername("username");
        utilisateur.setFirstname("firstname_save");
        utilisateur.setLastname("lastname");
        utilisateur.setGender("Female");

        Utilisateur saved = utilisateurRepository.save(utilisateur);

        assertEquals(saved.getFirstname(), "firstname_save");
    }

    @Test
    public void deleteById() {
        utilisateurRepository.deleteById(utilisateur1.getId());

        assertThat(utilisateurRepository.count()).isZero();
    }

}