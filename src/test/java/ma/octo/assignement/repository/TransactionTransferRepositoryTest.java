package ma.octo.assignement.repository;

import ma.octo.assignement.domain.TransactionTransfer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class TransactionTransferRepositoryTest {

  @Autowired
  private TransactionTransferRepository transferRepository;

  private TransactionTransfer transfer1;

  @BeforeEach
  public void beforeEachTest() {
    transfer1 = new TransactionTransfer();
    transfer1.setMontant(BigDecimal.valueOf(1000));
    transferRepository.deleteAll();
    transferRepository.save(transfer1);
    System.out.println(transfer1.getId());
  }

  @AfterEach
  public void afterEachTest() {
    transfer1 = null;
  }

  @Test
  public void findOne() {
    Optional<TransactionTransfer> result = transferRepository.findById(transfer1.getId());

    assertTrue(result.isPresent());
    assertEquals(transfer1.getId(), result.get().getId());
  }

  @Test
  public void findAll() {
    TransactionTransfer transfer2 = new TransactionTransfer();
    transfer2.setMontant(BigDecimal.valueOf(2000));
    transferRepository.save(transfer2);

    List<TransactionTransfer> result = transferRepository.findAll();

    assertNotNull(result);
    assertEquals(2, result.size());
  }

  @Test
  public void save() {
    TransactionTransfer transfer = new TransactionTransfer();
    transfer.setMontant(BigDecimal.valueOf(1234));

    TransactionTransfer saved = transferRepository.save(transfer);

    assertEquals(saved.getMontant().intValue(), 1234);
  }

  @Test
  public void deleteById() {
    transferRepository.deleteById(transfer1.getId());

    Optional<TransactionTransfer> result = transferRepository.findById(transfer1.getId());

    assertFalse(result.isPresent());
  }

}