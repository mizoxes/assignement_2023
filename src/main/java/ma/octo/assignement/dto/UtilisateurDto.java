package ma.octo.assignement.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UtilisateurDto {
    private String username;
    private String gender;
    private String lastname;
    private String firstname;
    private Date birthdate;
}
