package ma.octo.assignement.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
public class TransactionTransfer extends Transaction {
    @ManyToOne
    Compte compteEmetteur;
}