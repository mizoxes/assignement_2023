package ma.octo.assignement.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "COMPTE")
public class Compte {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(length = 16, unique = true)
  private String nrCompte;

  private String rib;

  @Column(precision = 16, scale = 2)
  private BigDecimal solde;

  @ManyToOne()
  @JoinColumn(name = "idUtilisateur")
  private Utilisateur utilisateur;

  @OneToMany(mappedBy = "compteEmetteur")
  Set<TransactionTransfer> transfersEmetteur;

  @OneToMany(mappedBy = "compteBeneficiaire")
  Set<TransactionTransfer> transfersBeneficiaire;

  @OneToMany(mappedBy = "compteBeneficiaire")
  Set<TransactionDeposit> depositsBeneficiaire;

  @PreRemove
  public void remove() {
    transfersEmetteur.forEach(transfer -> transfer.setCompteEmetteur(null));
    transfersBeneficiaire.forEach(transfer -> transfer.setCompteBeneficiaire(null));
    depositsBeneficiaire.forEach(deposit -> deposit.setCompteBeneficiaire(null));
  }
}
