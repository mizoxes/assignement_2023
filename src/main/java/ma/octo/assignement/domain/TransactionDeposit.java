package ma.octo.assignement.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
public class TransactionDeposit extends Transaction {
    @Column
    private String nomEmetteur;

    @Column
    private String rib;
}
