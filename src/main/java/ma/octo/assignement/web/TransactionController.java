package ma.octo.assignement.web;

import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exception.CompteNonExistantException;
import ma.octo.assignement.exception.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exception.TransactionException;
import ma.octo.assignement.mapper.DepositMapper;
import ma.octo.assignement.mapper.TransferMapper;
import ma.octo.assignement.service.ITransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController()
@RequestMapping("/transaction")
@Slf4j
public class TransactionController {

    @Autowired
    private ITransactionService transactionService;

    @GetMapping("listerTransferts")
    List<TransferDto> listerTranferts() {
        log.info("Lister des transfers");
        return transactionService.getAllTransfers()
                .stream()
                .map(TransferMapper::map)
                .collect(toList());
    }

    @GetMapping("listerDeposits")
    List<DepositDto> listerDeposits() {
        log.info("Lister des deposits");
        return transactionService.getAllDeposits()
                .stream()
                .map(DepositMapper::map)
                .collect(toList());
    }

    @PostMapping("/executerTransfert")
    @ResponseStatus(HttpStatus.CREATED)
    public void executerTransfert(@RequestBody TransferDto transferDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
        transactionService.doTransfer(transferDto);
    }

    @PostMapping("/executerDeposit")
    @ResponseStatus(HttpStatus.CREATED)
    public void executerDeposit(@RequestBody DepositDto depositDto)
            throws CompteNonExistantException, TransactionException {
        transactionService.doDeposit(depositDto);
    }

}
