package ma.octo.assignement.web;

import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.mapper.CompteMapper;
import ma.octo.assignement.service.ICompteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController()
@RequestMapping("/compte")
@Slf4j
public class CompteController {

    @Autowired
    private ICompteService compteService;

    @GetMapping("listerComptes")
    List<CompteDto> listerComptes() {
        log.info("Lister des comptes");
        return compteService.getAll()
                .stream()
                .map(CompteMapper::map)
                .collect(toList());
    }

}
