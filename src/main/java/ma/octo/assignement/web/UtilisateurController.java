package ma.octo.assignement.web;

import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.mapper.UtilisateurMapper;
import ma.octo.assignement.service.IUtilisateurService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping("/utilisateur")
@Slf4j
public class UtilisateurController {
    @Autowired
    private IUtilisateurService utilisateurService;

    @GetMapping("listerUtilisateurs")
    public List<UtilisateurDto> listerUtilisateurs() {
        log.info("Lister des utilisateurs");
        return utilisateurService.getAll()
                .stream()
                .map(UtilisateurMapper::map)
                .collect(toList());
    }

}
