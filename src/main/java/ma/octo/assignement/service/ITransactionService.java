package ma.octo.assignement.service;

import ma.octo.assignement.domain.TransactionDeposit;
import ma.octo.assignement.domain.TransactionTransfer;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exception.CompteNonExistantException;
import ma.octo.assignement.exception.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exception.TransactionException;

import java.util.List;

public interface ITransactionService {

    void doTransfer(TransferDto transferDto) throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException;

    void doDeposit(DepositDto depositDto) throws CompteNonExistantException, TransactionException;

    List<TransactionTransfer> getAllTransfers();

    List<TransactionDeposit> getAllDeposits();

}
