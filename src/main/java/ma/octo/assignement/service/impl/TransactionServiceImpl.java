package ma.octo.assignement.service.impl;

import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.domain.*;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exception.CompteNonExistantException;
import ma.octo.assignement.exception.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exception.TransactionException;
import ma.octo.assignement.repository.AuditRepository;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransactionDepositRepository;
import ma.octo.assignement.repository.TransactionTransferRepository;
import ma.octo.assignement.service.ITransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static ma.octo.assignement.constants.Constants.*;

@Service
@Transactional
@Slf4j
public class TransactionServiceImpl implements ITransactionService {

    @Autowired
    private TransactionDepositRepository depositRepository;
    @Autowired
    private TransactionTransferRepository transferRepository;
    @Autowired
    private CompteRepository compteRepository;
    @Autowired
    private AuditRepository auditRepository;

    @Override
    public void doTransfer(TransferDto transferDto) throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException {
        Compte compteEmetteur       = compteRepository.findByNrCompte(transferDto.getNrCompteEmetteur());
        Compte compteBeneficiaire   = compteRepository.findByNrCompte(transferDto.getNrCompteBeneficiaire());

        if (compteEmetteur == null || compteBeneficiaire == null) {
            log.error("Compte Non existant");
            throw new CompteNonExistantException("Compte introuvable");
        }

        validateTransaction(transferDto.getMontant(), transferDto.getMotif());

        if (IsLess(compteEmetteur.getSolde(), transferDto.getMontant())) {
            log.error("Solde insuffisant");
            throw new SoldeDisponibleInsuffisantException("Solde insuffisant");
        }

        compteEmetteur.setSolde(
                compteEmetteur.getSolde().subtract(transferDto.getMontant())
        );
        compteRepository.save(compteEmetteur);

        compteBeneficiaire.setSolde(
                compteBeneficiaire.getSolde().add(transferDto.getMontant())
        );
        compteRepository.save(compteBeneficiaire);

        TransactionTransfer transfer = new TransactionTransfer();
        transfer.setDateExecution(new Date());
        transfer.setCompteBeneficiaire(compteBeneficiaire);
        transfer.setCompteEmetteur(compteEmetteur);
        transfer.setMontant(transferDto.getMontant());
        transfer.setMotif(transferDto.getMotif());
        transferRepository.save(transfer);

        auditTransfer("Transfer depuis " + transferDto.getNrCompteEmetteur() + " vers " +
                transferDto.getNrCompteBeneficiaire() + " d'un montant de " + transferDto.getMontant().toString());
    }

    @Override
    public void doDeposit(DepositDto depositDto) throws CompteNonExistantException, TransactionException {
        Compte compteBeneficiaire = compteRepository.findByRib(depositDto.getRib());

        if (compteBeneficiaire == null) {
            log.error("Compte Non existant");
            throw new CompteNonExistantException();
        }

        validateTransaction(depositDto.getMontant(), depositDto.getMotif());

        // empêcher l'emetteur de déposer plus de 100000 par jour
        Date date = new Date();
        date.setHours(0);
        BigDecimal depositAmountOfToday = depositRepository.getTotalDepositByNomEmetteurAndDateExecution(depositDto.getNomEmetteur(), date);
        if (depositAmountOfToday != null && IsGreater(depositAmountOfToday.add(depositDto.getMontant()), MAX_DEPOSIT_AMOUNT_PER_DAY)) {
            String msg = "Vous ne pouvez pas depasser le montant maximale pour aujourd'hui: " + depositAmountOfToday + "/" + MAX_DEPOSIT_AMOUNT_PER_DAY;
            log.error(msg);
            throw new TransactionException(msg);
        }

        compteBeneficiaire.setSolde(
                compteBeneficiaire.getSolde().add(depositDto.getMontant())
        );
        compteRepository.save(compteBeneficiaire);

        TransactionDeposit deposit = new TransactionDeposit();
        deposit.setDateExecution(new Date());
        deposit.setNomEmetteur(depositDto.getNomEmetteur());
        deposit.setMontant(depositDto.getMontant());
        deposit.setRib(depositDto.getRib());
        deposit.setCompteBeneficiaire(compteBeneficiaire);
        deposit.setMotif(depositDto.getMotif());
        depositRepository.save(deposit);

        auditDeposit("Deposit dans " + depositDto.getRib() + " d'un montant de " +
                depositDto.getMontant().toString());
    }

    @Override
    public List<TransactionTransfer> getAllTransfers() {
        return transferRepository.findAll();
    }

    @Override
    public List<TransactionDeposit> getAllDeposits() {
        return depositRepository.findAll();
    }

    private void validateTransaction(BigDecimal montant, String motif) throws TransactionException {
        if (montant == null || IsZero(montant)) {
            log.error("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (IsLess(montant, MIN_TRANSACTION_AMOUNT)) {
            log.error("Montant minimal de transfer non atteint");
            throw new TransactionException("Montant minimal de transfer non atteint");
        } else if (IsGreater(montant, MAX_TRANSACTION_AMOUNT)) {
            log.error("Montant maximal de transfer dépassé");
            throw new TransactionException("Montant maximal de transfer dépassé");
        }

        if (motif == null || motif.isBlank()) {
            log.error("Motif vide");
            throw new TransactionException("Motif vide");
        }
    }

    private void auditTransfer(String message) {
        log.info("Audit de l'événement TRANSFER");
        AuditTransfer audit = new AuditTransfer();
        audit.setMessage(message);
        auditRepository.save(audit);
    }

    private void auditDeposit(String message) {
        log.info("Audit de l'événement DEPOSIT");
        AuditDeposit audit = new AuditDeposit();
        audit.setMessage(message);
        auditRepository.save(audit);
    }

    private boolean IsGreater(BigDecimal a, BigDecimal b) {
        return a.compareTo(b) > 0;
    }

    private boolean IsLess(BigDecimal a, BigDecimal b) {
        return a.compareTo(b) < 0;
    }

    private boolean IsZero(BigDecimal a) {
        return a.compareTo(BigDecimal.ZERO) == 0;
    }

}
