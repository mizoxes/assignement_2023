package ma.octo.assignement.repository;

import ma.octo.assignement.domain.TransactionDeposit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.Date;

public interface TransactionDepositRepository extends JpaRepository<TransactionDeposit, Long> {
    @Query(value = "SELECT SUM(montant) FROM TRANSACTIONDEPOSIT WHERE nomEmetteur=:nomEmetteur AND dateExecution >= :date", nativeQuery = true)
    BigDecimal getTotalDepositByNomEmetteurAndDateExecution(@Param("nomEmetteur") String nomEmetteur, @Param("date") Date date);
}