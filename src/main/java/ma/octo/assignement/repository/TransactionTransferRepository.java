package ma.octo.assignement.repository;

import ma.octo.assignement.domain.TransactionTransfer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionTransferRepository extends JpaRepository<TransactionTransfer, Long> {
}
