package ma.octo.assignement.constants;

import java.math.BigDecimal;

public class Constants {
    public static final BigDecimal MIN_TRANSACTION_AMOUNT = new BigDecimal(10);
    public static final BigDecimal MAX_TRANSACTION_AMOUNT = new BigDecimal(10000);
    public static final BigDecimal MAX_DEPOSIT_AMOUNT_PER_DAY = new BigDecimal(100000);
}
