package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.TransactionTransfer;
import ma.octo.assignement.dto.TransferDto;

public class TransferMapper {

    public static TransferDto map(TransactionTransfer transfer) {
        TransferDto transferDto = new TransferDto();

        transferDto.setNrCompteEmetteur(transfer.getCompteEmetteur().getNrCompte());
        transferDto.setNrCompteBeneficiaire(transfer.getCompteBeneficiaire().getNrCompte());
        transferDto.setMotif(transfer.getMotif());
        transferDto.setMontant(transfer.getMontant());

        return transferDto;

    }

}
