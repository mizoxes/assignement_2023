package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.CompteDto;

public class CompteMapper {

    public static CompteDto map(Compte compte) {
        CompteDto compteDto = new CompteDto();

        compteDto.setNrCompte(compte.getNrCompte());
        compteDto.setRib(compte.getRib());
        compteDto.setSolde(compte.getSolde());

        return compteDto;
    }

}