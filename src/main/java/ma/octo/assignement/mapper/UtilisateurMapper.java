package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.UtilisateurDto;

public class UtilisateurMapper {

    public static UtilisateurDto map(Utilisateur utilisateur) {
        UtilisateurDto utilisateurDto = new UtilisateurDto();

        utilisateurDto.setUsername(utilisateur.getUsername());
        utilisateurDto.setFirstname(utilisateur.getFirstname());
        utilisateurDto.setLastname(utilisateur.getLastname());
        utilisateurDto.setGender(utilisateur.getGender());
        utilisateurDto.setBirthdate(utilisateur.getBirthdate());

        return utilisateurDto;

    }

}
