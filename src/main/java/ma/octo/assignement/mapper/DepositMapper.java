package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.TransactionDeposit;
import ma.octo.assignement.dto.DepositDto;

public class DepositMapper {

    public static DepositDto map(TransactionDeposit deposit) {
        DepositDto depositDto = new DepositDto();

        depositDto.setRib(deposit.getRib());
        depositDto.setNomEmetteur(deposit.getNomEmetteur());
        depositDto.setMotif(deposit.getMotif());
        depositDto.setMontant(deposit.getMontant());

        return depositDto;

    }
}
